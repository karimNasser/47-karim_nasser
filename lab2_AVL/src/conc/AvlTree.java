package conc;

import eg.edu.alexu.csd.filestructure.avl.IAVLTree;
import eg.edu.alexu.csd.filestructure.avl.INode;

public class AvlTree<T extends Comparable<T>> implements IAVLTree<T> {

  private Node<T> root;

  public AvlTree() {
    root = null;
  }

  private Node<T> rightRotate(Node<T> node) {

    Node<T> leftChild = (Node<T>) node.getLeftChild();
    Node<T> temp = (Node<T>) leftChild.getRightChild();

    leftChild.setRightChild(node);
    node.setLeftChild(temp);

    node.setHeight(Math.max(getHeight((Node<T>) node.getLeftChild()),
        getHeight((Node<T>) node.getRightChild())) + 1);

    leftChild.setHeight(Math.max(getHeight((Node<T>) leftChild.getLeftChild()),
        getHeight((Node<T>) leftChild.getRightChild())) + 1);
    return leftChild;
  }

  private Node<T> leftRotate(Node<T> node) {

    Node<T> rightChild = (Node<T>) node.getRightChild();
    Node<T> temp = (Node<T>) rightChild.getLeftChild();

    rightChild.setLeftChild(node);
    node.setRightChild(temp);

    node.setHeight(Math.max(getHeight((Node<T>) node.getLeftChild()),
        getHeight((Node<T>) node.getRightChild())) + 1);

    rightChild.setHeight(Math.max(getHeight((Node<T>) rightChild.getLeftChild()),
        getHeight((Node<T>) rightChild.getRightChild())) + 1);
    return rightChild;
  }

  private int getHeight(Node<T> node) {
    if (node == null) {
      return 0;
    }

    return node.getHeight();
  }

  private int getBalance(Node<T> node) {

    Node<T> leftChild = (Node<T>) node.getLeftChild();
    Node<T> rightChild = (Node<T>) node.getRightChild();

    return getHeight(leftChild) - getHeight(rightChild);
  }

  @Override
  public void insert(T key) {
    root = insert(root, key);
  }

  private Node<T> insert(Node<T> node, T key) {

    if (node == null) {
      return new Node<T>(null, null, key, 1);
    }

    if (key.compareTo(node.getValue()) < 0) {
      node.setLeftChild(insert((Node<T>) node.getLeftChild(), key));

    } else if (key.compareTo(node.getValue()) >= 0) {
      node.setRightChild(insert((Node<T>) node.getRightChild(), key));
    }
    /*else {
      return node;
    }
*/
    Node<T> leftChild = (Node<T>) node.getLeftChild();
    Node<T> rightChild = (Node<T>) node.getRightChild();

    node.setHeight(Math.max(getHeight(leftChild), getHeight(rightChild)) + 1);

    int balance = getBalance(node);

    if (balance > 1 && getBalance(leftChild) > 0) {
      return rightRotate(node);
    }
    if (balance > 1 && getBalance(leftChild) < 0) {
      ((Node<T>) node).setLeftChild(leftRotate(leftChild));
      return rightRotate(node);
    }

    if (balance < -1 && getBalance(rightChild) < 0) {
      return leftRotate(node);
    }
    if (balance < -1 && getBalance(rightChild) > 0) {
      ((Node<T>) node).setRightChild(rightRotate(rightChild));
      return leftRotate(node);
    }

    return node;

  }

  private Node<T> getMinNode(Node<T> node) {
    if (node == null) {
      return node;
    }
    if (node.getLeftChild() == null) {
      return node;
    }
    return getMinNode((Node<T>) node.getLeftChild());
  }

  @Override
  public boolean delete(T key) {

    boolean exists = search(key);
    root = delete(root, key);
    return exists;
  }

  private Node<T> delete(Node<T> node, T key) {

    if (node == null) {
      return node;
    }
    Node<T> leftChild = (Node<T>) node.getLeftChild();
    Node<T> rightChild = (Node<T>) node.getRightChild();

    if (key.compareTo(node.getValue()) < 0) {
      leftChild = delete((Node<T>) node.getLeftChild(), key);
      node.setLeftChild(leftChild);
    } else if (key.compareTo(node.getValue()) > 0) {
      rightChild = delete((Node<T>) node.getRightChild(), key);
      node.setRightChild(rightChild);
    } else {

      if (leftChild == null && rightChild == null) {
        return null;
      }
      if (leftChild == null) {
        return rightChild;
      } else if (rightChild == null) {
        return leftChild;
      }

      Node<T> temp = getMinNode(rightChild);
      node.setValue(temp.getValue());
      rightChild = delete(rightChild, temp.getValue());
      node.setRightChild(rightChild);
    }

    node.setHeight(Math.max(getHeight(leftChild), getHeight(rightChild)) + 1);

    int balance = getBalance(node);

    if (balance > 1 && getBalance(leftChild) >= 0) {
      return rightRotate(node);
    }
    if (balance > 1 && getBalance(leftChild) < 0) {
      node.setLeftChild(leftRotate(leftChild));
      return rightRotate(node);
    }

    if (balance < -1 && getBalance(rightChild) <= 0) {
      return leftRotate(node);
    }
    if (balance < -1 && getBalance(rightChild) > 0) {
      node.setRightChild(rightRotate(rightChild));
      return leftRotate(node);
    }

    return node;
  }

  @Override
  public boolean search(T key) {
    return search(root, key);
  }

  private boolean search(Node<T> node, T key) {

    if (node == null) {
      return false;
    }

    if (key.compareTo(node.getValue()) < 0) {
      return search((Node<T>) node.getLeftChild(), key);
    }
    if (key.compareTo(node.getValue()) > 0) {
      return search((Node<T>) node.getRightChild(), key);
    }

    return true;
  }

  @Override
  public int height() {
    return getHeight(root);
  }

  @Override
  public INode<T> getTree() {
    return root;
  }

  public int size() {
    return size(root);
  }

  private int size(Node<T> node) {
    if (node == null) {
      return 0;
    }
    return 1 + size((Node<T>) node.getLeftChild()) + size((Node<T>) node.getRightChild());
  }

}
