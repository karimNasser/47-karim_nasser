package conc;

import eg.edu.alexu.csd.filestructure.avl.IDictionary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary {

  private AvlTree<String> avlTree;

  public Dictionary() {
    avlTree = new AvlTree<>();
  }

  @Override
  public void load(File file) {

    BufferedReader reader;

    try {
      reader = new BufferedReader(new FileReader(file));
    } catch (FileNotFoundException e1) {
      return;
    }

    String word;
    try {
      while ((word = reader.readLine()) != null) {
        insert(word);
      }
      reader.close();
    } catch (IOException e1) {
      
      return;
    }
  }

  @Override
  public boolean insert(String word) {

    if (avlTree.search(word)) {
      return false;
    }

    avlTree.insert(word);
    return true;
  }

  @Override
  public boolean exists(String word) {
    return avlTree.search(word);
  }

  @Override
  public boolean delete(String word) {
    return avlTree.delete(word);
  }

  @Override
  public int size() {
    return avlTree.size();
  }

  @Override
  public int height() {
    return avlTree.height();
  }

}
