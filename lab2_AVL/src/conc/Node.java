package conc;

import eg.edu.alexu.csd.filestructure.avl.INode;

public class Node<T extends Comparable<T>> implements INode<T> {

  private T val;
  private Node<T> leftChild;
  private Node<T> rightChild;
  private int height;

  /**
   * 
   * @param left
   *          left child
   * @param right
   *          right child
   * @param val
   *          value
   * @param height node height
   */
  public Node(Node<T> left, Node<T> right, T val, int height) {

    this.leftChild = left;
    this.rightChild = right;
    this.val = val;
    this.height = height;

  }

  public void setLeftChild(Node<T> node) {
    this.leftChild = node;
  }

  public void setRightChild(Node<T> node) {
    this.rightChild = node;
  }

  public void setHeight(int hh) {
    this.height = hh;
  }

  public int getHeight() {
    return height;
  }

  @Override
  public INode<T> getLeftChild() {
    return leftChild;
  }

  @Override
  public INode<T> getRightChild() {
    // TODO Auto-generated method stub
    return rightChild;
  }

  @Override
  public T getValue() {
    return val;
  }

  @Override
  public void setValue(T value) {
    this.val = value;
  }

}
