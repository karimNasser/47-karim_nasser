package eg.edu.alexu.csd.filestructure.sort.concrete;

import java.util.ArrayList;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

public class Sorter<T extends Comparable<T>> implements ISort<T> {

  @Override
  public IHeap<T> heapSort(ArrayList<T> unordered) {

    MaxHeap<T> myHeap = new MaxHeap<T>();
    return myHeap.heapSort(unordered);

  }

  @Override
  public void sortSlow(ArrayList<T> unordered) {

    for (int i = 1; i < unordered.size(); i++) {

      int current = i;
      while (current > 0 && unordered.get(current).compareTo(unordered.get(current - 1)) < 0) {

        swap(unordered, current, current - 1);
        current--;
      }

    }

  }

  @Override
  public void sortFast(ArrayList<T> unordered) {

    quickSort(unordered, 0, unordered.size() - 1);

  }

  private void quickSort(ArrayList<T> list, int lo, int hi) {

    if (lo >= hi)
      return;

    int p = partition(list, lo, hi);
    quickSort(list, lo, p);
    quickSort(list, p + 1, hi);

  }

  private int partition(ArrayList<T> list, int lo, int hi) {

    T pivot = list.get(lo);
    int i = lo - 1;
    int j = hi + 1;

    while (true) {

      do {
        i++;
      } while (list.get(i).compareTo(pivot) < 0);

      do {
        j--;
      } while (list.get(j).compareTo(pivot) > 0);

      if (i >= j)
        return j;
      swap(list, i, j);

    }

  }

  private void swap(ArrayList<T> unordered, int first, int second) {

    T temp = unordered.get(first);
    unordered.set(first, unordered.get(second));

    unordered.set(second, temp);
  }

}
