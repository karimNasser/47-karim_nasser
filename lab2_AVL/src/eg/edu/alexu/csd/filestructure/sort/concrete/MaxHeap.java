package eg.edu.alexu.csd.filestructure.sort.concrete;

import java.nio.channels.NetworkChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;
import eg.edu.alexu.csd.filestructure.sort.ISort;

public class MaxHeap<T extends Comparable<T>> implements IHeap<T> {

  private int heapSize = 0;
  private int maxSize;
  private INode<T>[] heapArr;

  public MaxHeap(int maxSize) {

    if (maxSize >= 20)
      this.maxSize = maxSize;
    else
      this.maxSize = 100;

    heapArr = (INode<T>[]) new INode[maxSize];

  }

  public MaxHeap() {
    this(100);
  }

  @Override
  public INode<T> getRoot() {
    if (heapSize == 0) {
      throw new RuntimeException("heap is empty");
    }

    return heapArr[0];
  }

  @Override
  public int size() {
    return heapSize;
  }

  @Override
  public void heapify(INode<T> node) {

    if (node == null)
      return;
    if (heapSize == 0) {
      throw new RuntimeException("null node");
    }

    if (isLeaf(node))
      return;

    INode<T> greaterChild = node.getLeftChild();
    if (node.getRightChild() != null) {
      greaterChild = getGreater(node.getLeftChild(), node.getRightChild());
    }

    if (getGreater(node, greaterChild) == greaterChild) {

      swap(node, greaterChild);
      heapify(greaterChild);
    }

  }

  private void swap(INode<T> node1, INode<T> node2) {

    T temp = node1.getValue();
    node1.setValue(node2.getValue());
    node2.setValue(temp);

  }

  private INode<T> getGreater(INode<T> node1, INode<T> node2) {

    if (node1.getValue().compareTo(node2.getValue()) > 0) {
      return node1;
    }

    return node2;

  }

  private boolean isLeaf(INode<T> node) {
    return node.getLeftChild() == null && node.getRightChild() == null;
  }

  @Override
  public T extract() {

    if (heapSize <= 0)
      throw new RuntimeException("heap is empty");

    T max = heapArr[0].getValue();
    if (heapSize == 1) {
      heapSize--;
      return max;
    }

    swap(heapArr[0], heapArr[heapSize - 1]);
    heapSize--;
    heapify(heapArr[0]);

    return max;
  }

  @Override
  public void insert(T element) {

    if (element == null) {
      throw new RuntimeException("cannot insert null element");
    }

    if (heapSize == maxSize)
      extendArray();
    Node newNode = new Node(element, heapSize);
    heapArr[heapSize] = newNode;
    heapSize++;
    insert(newNode);

  }

  private void extendArray() {

    int newSize = maxSize * 2;
    INode<T>[] newArr = (INode<T>[]) new INode[newSize];
    System.arraycopy(heapArr, 0, newArr, 0, maxSize);
    maxSize = newSize;
    heapArr = newArr;

  }

  private void insert(INode<T> node) {

    INode<T> parent = node.getParent();
    if (parent == null)
      return; // root

    if (getGreater(node, parent) == node) {

      swap(node, parent);
      insert(parent);
    }

  }

  @Override
  public void build(Collection<T> unordered) {

    maxSize = Math.max(unordered.size(), maxSize);
    heapSize = unordered.size();
    heapArr = (INode<T>[]) new INode[maxSize];

    T[] temp = (T[]) new Comparable[unordered.size()];
    unordered.toArray(temp);
    for (int i = 0; i < unordered.size(); i++) {
      heapArr[i] = new Node(temp[i], i);
    }

    buildHeap(heapArr);
  }

  private void buildHeap(INode<T>[] heap) {

    int size = heap.length;
    for (int i = size - 1; i >= 0; i--) {
      heapify(heap[i]);
    }

  }

  public IHeap<T> clone() {

    MaxHeap<T> myHeap = new MaxHeap<T>();
    INode<T>[] newArr = (INode<T>[]) new INode[maxSize];

    for (int i = 0; i < heapSize; i++) {
      newArr[i] = myHeap.new Node(heapArr[i].getValue(), i);
    }

    myHeap.heapArr = newArr;
    myHeap.heapSize = heapSize;
    myHeap.maxSize = maxSize;

    return myHeap;

  }

  public IHeap<T> heapSort(ArrayList<T> list) {

    build(list);

    for (int i = heapSize - 1; i >= 1; i--) {

      T max = extract();
      heapArr[i] = new Node(max, i);

    }

    for (int i = 0; i < list.size(); i++) {
      list.set(i, heapArr[i].getValue());
    }

    heapSize = list.size();
    return this;
  }

  private class Node implements INode<T> {

    private T element;
    private int index;

    public Node(T element, int index) {
      this.element = element;
      this.index = index;
    }

    @Override
    public INode<T> getLeftChild() {

      int lChild = index * 2 + 1;
      if (lChild >= MaxHeap.this.heapSize) {
        return null;
      }

      return heapArr[lChild];
    }

    @Override
    public INode<T> getRightChild() {

      int rChild = index * 2 + 2;
      if (rChild >= MaxHeap.this.heapSize) {
        return null;
      }

      return heapArr[rChild];

    }

    @Override
    public INode<T> getParent() {

      if (index == 0)
        return null; // is the root
      if (index >= MaxHeap.this.heapSize) {
        throw new RuntimeException("element not found");
      }

      int parent = (index - 1) / 2;
      return heapArr[parent];

    }

    @Override
    public T getValue() {

      return element;
    }

    @Override
    public void setValue(T value) {

      this.element = value;

    }

    @Override
    public String toString() {
      return element.toString() + "(" + index + ")";
    }

  }

}
