package eg.edu.alexu.csd.filestructure.sort.concrete;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

public class tester {

  @Test
  public void test() {

    Sorter<Integer> s = new Sorter<>();

    ArrayList<Integer> l = new ArrayList<>();

    l.add(1);
    l.add(3);
    l.add(2);
    l.add(6);
    l.add(5);

    IHeap<Integer> h = s.heapSort(l);

    System.out.println(h.size());
    System.out.println(h.getRoot().getValue());

    for (int i = 0; i < l.size(); i++) {
      System.out.println(l.get(i));
    }

  }

}
