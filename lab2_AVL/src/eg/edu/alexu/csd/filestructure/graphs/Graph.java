package eg.edu.alexu.csd.filestructure.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;

import javax.management.RuntimeErrorException;

import java.util.Hashtable;
import java.util.Iterator;

public class Graph implements IGraph {

  private List<List<Pair>> adjList = null;
  private int numberOfVertices;
  private int numberOfEdges;
  private ArrayList<Integer> processingOrder;

  public Graph() {

  }

  @Override
  public void readGraph(File file) {

    try {
      BufferedReader inputFile = new BufferedReader(new FileReader(file));
      String line;
      processingOrder = null; // no dijkstra was run on new graph

      // read no of vertices and edges
      line = inputFile.readLine();
      String[] splitLine = line.trim().split("\\s+");
      numberOfVertices = Integer.parseInt(splitLine[0]);
      numberOfEdges = Integer.parseInt(splitLine[1]);
      adjList = adjList = new ArrayList<List<Pair>>(numberOfVertices);
      initializeAdjList();
 /*     
      if (true) {
        throw new RuntimeException(line);
      }
*/
      // read edges
      int edgesRead = 0;
      while ((line = inputFile.readLine()) != null) {
        int source, destination;
        int edgeWeight;

        // extract vertices and edge weight
        splitLine = line.trim().split("\\s+");
        source = Integer.parseInt(splitLine[0]);
        destination = Integer.parseInt(splitLine[1]);
        edgeWeight = Integer.parseInt(splitLine[2]);
        
        if (!inVertexRange(source) || !inVertexRange(destination)) {
          throw new RuntimeException("wrong file content" + "-vetex is out of range");
        }
        adjList.get(source).add(new Pair(destination, edgeWeight));
        ++edgesRead;
      }
      if (edgesRead != numberOfEdges) {
        throw new RuntimeException("incomplete edge list");
      }
    } catch (RuntimeException error) {
      throw error;
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("error");
    }

  }

  private void initializeAdjList() {
    for (int i = 0; i < numberOfVertices; ++i) {
      adjList.add(new ArrayList<Pair>());
    }
  }

  private boolean inVertexRange(int vertex) {
    return vertex >= 0 && vertex < numberOfVertices;
  }

  // =====================================================

  @Override
  public int size() {
    if (adjList.isEmpty()) {
      throw new RuntimeException("no size for empty list");
    }
    return numberOfEdges;
  }

  @Override
  public ArrayList<Integer> getVertices() {
    if (adjList.isEmpty()) {
      throw new RuntimeException("no getVertices for empty list");
    }
    ArrayList<Integer> vertices = new ArrayList<Integer>();
    for (int i = 0; i < numberOfVertices; ++i) {
      vertices.add(i);
    }
    return vertices;
  }

  @Override
  public ArrayList<Integer> getNeighbors(int v) {
    if (adjList.isEmpty()) {
      throw new RuntimeException("no getNeighbours for empty list");
    }
    if (!inVertexRange(v)) {
      throw new RuntimeException("vertex is out of range");
    }

    ArrayList<Integer> neighbours = new ArrayList<>();
    ListIterator<Pair> iter = adjList.get(v).listIterator();
    while (iter.hasNext()) {
      neighbours.add(iter.next().vertex);
    }

    return neighbours;
  }

  // ============== dijkstra ======================

  private void initializeDistArray(int[] distances) {
    for (int i = 0; i < distances.length; ++i) {
      distances[i] = Integer.MAX_VALUE / 2;
    }
  }

  private boolean relaxEdge(int[] distances, int src, Pair destination) {
    if ((long) distances[destination.vertex] > (long) distances[src] + (long) destination.weight) {
      distances[destination.vertex] = distances[src] + destination.weight;
      return true;
    }
    return false;
  }

  private ArrayList<Integer> dijkstraWithProcessOrder(int src, int[] distances) {

    if (distances.length < numberOfVertices) {
      throw new RuntimeException("too small array");
    }
    
    if (adjList.isEmpty()) {
      throw new RuntimeException("error");
    }

    // initializing distances to infinity
    initializeDistArray(distances);
    distances[src] = 0;

    boolean[] isVisited = new boolean[numberOfVertices]; // initialized to
                                                         // false by default
    PriorityQueue<Pair> pQueue = new PriorityQueue<>();
    ArrayList<Integer> processingOrder;
    processingOrder = new ArrayList<>();

    // push source node
    pQueue.offer(new Pair(src, 0));

    while (!pQueue.isEmpty()) {
      Pair currentPair = pQueue.poll();
      if (isVisited[currentPair.vertex]) {
        continue;
      }
      isVisited[currentPair.vertex] = true;
      processingOrder.add(currentPair.vertex);
      Iterator<Pair> iter = adjList.get(currentPair.vertex).iterator();
      while (iter.hasNext()) {
        Pair adjacentVertex = iter.next();
        if (!isVisited[adjacentVertex.vertex]) {
          relaxEdge(distances, currentPair.vertex, adjacentVertex);
          pQueue.offer(new Pair(adjacentVertex.vertex, distances[adjacentVertex.vertex]));
        }
      }

    }
    return processingOrder;
  }

  @Override
  public void runDijkstra(int src, int[] distances) {
    processingOrder = dijkstraWithProcessOrder(src, distances);
  }

  @Override
  public ArrayList<Integer> getDijkstraProcessedOrder() {
    if (processingOrder == null) {
      return null;
      //throw new RuntimeException("error");
    }
    return processingOrder;
  }

  // ============== bellman-ford ======================

  @Override
  public boolean runBellmanFord(int src, int[] distances) {
    
    if (distances.length < numberOfVertices) {
      throw new RuntimeException("too small array");
    }
    if (adjList.isEmpty()) {
      throw new RuntimeException("no graph found");
    }
    
    initializeDistArray(distances);
    distances[src] = 0;

    int numberOfRelaxations = numberOfVertices - 1;
    while (numberOfRelaxations > 0) {

      GraphIterator edgeList = new GraphIterator(adjList);
      while (edgeList.hasNext()) {
        Triple edge = edgeList.next();
        if (edge == null) {
          continue;
        }
        relaxEdge(distances, edge.src, new Pair(edge.dest, edge.weight));
      }

      --numberOfRelaxations;
    }

    // one more relaxation trial
    GraphIterator edgeList = new GraphIterator(adjList);
    while (edgeList.hasNext()) {
      Triple edge = edgeList.next();
      if (edge == null) {
        continue;
      }
      boolean relaxed = relaxEdge(distances, edge.src, new Pair(edge.dest, edge.weight));
      if (relaxed) {
        return false;
      }
    }

    return true;
  }

  
  
  
  //========================================================
  private class Pair implements Comparable<Pair> {
    int vertex;
    int weight;

    public Pair(int vertex, int weight) {
      this.vertex = vertex;
      this.weight = weight;
    }

    @Override
    public int compareTo(Pair pairToCompare) {
      return weight - pairToCompare.weight;
    }

  }

  private class Triple {

    private int src;
    private int dest;
    private int weight;

    public Triple(int src, int dest, int weight) {
      this.src = src;
      this.dest = dest;
      this.weight = weight;
    }

  }

  private class GraphIterator implements Iterator<Triple> {

    private List<List<Pair>> adjList;
    private Iterator<List<Pair>> mainVerticesIter;
    private Iterator<Pair> adjVerticesIter;
    private int currentVertex;

    public GraphIterator(List<List<Pair>> adjList) {
      this.adjList = adjList;
      mainVerticesIter = adjList.iterator();
      adjVerticesIter = null;
      currentVertex = -1;
    }

    @Override
    public boolean hasNext() {
      return mainVerticesIter.hasNext() || (adjVerticesIter != null && adjVerticesIter.hasNext());
    }

    @Override
    public Triple next() {
      if (adjVerticesIter == null || !adjVerticesIter.hasNext()) {
        if (mainVerticesIter.hasNext()) {
          adjVerticesIter = mainVerticesIter.next().iterator();
          ++currentVertex;
        } else {
          return null;
        }
      }
      if (!adjVerticesIter.hasNext()) {
        return null;
      }
      Pair temp = adjVerticesIter.next();
      return new Triple(currentVertex, temp.vertex, temp.weight);
    }

  }

}
