package eg.edu.alexu.csd.filestructure.graphs;

import java.io.File;

public class Tester {
  public static void main(String[] args) {
    File f = new File("C:/Users/Me/Desktop/a.txt");
    Graph g = new Graph();
    g.readGraph(f);
    int[] dist = new int[10];
    System.out.println(g.runBellmanFord(0, dist));
  }
}
