package eg.edu.alexu.csd.filestructure.hash.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import eg.edu.alexu.csd.filestructure.hash.IHash;

public abstract class ProbedHash implements IHash<Integer, String> {

  private final int DEFAULT_INITIAL_CAPACITY = 1200;
  private final double DEFAULT_EXTEND_FACTOR = 2;

  private int size;
  protected int capacity;
  private Triple[] map;
  private int numCollisions;

  private int ins = 0;
  private int del = 0;

  @SuppressWarnings("unchecked")
  public ProbedHash() {
    capacity = DEFAULT_INITIAL_CAPACITY;
    size = 0;
    numCollisions = 0;
    map = new Triple[DEFAULT_INITIAL_CAPACITY];
  }

  @Override
  public void put(Integer key, String value) {

    ++ins;

    Triple toHash = new Triple(key, value, State.Available);

    for (int i = 0; i < capacity; ++i) {
      int hashVal = hashFunction(key, i);
      if (map[hashVal] == null || map[hashVal].state == State.DELETED) {
        map[hashVal] = toHash;
        ++size;
        if (size == capacity) {
          extendTable(DEFAULT_EXTEND_FACTOR);
        }
        return;
      } else if (map[hashVal] != null && map[hashVal].equals(toHash)) {
        map[hashVal].second = value;
        if (size == capacity) {
          extendTable(DEFAULT_EXTEND_FACTOR);
        }
        return;
      }
      numCollisions++;
    }
    extendTable(DEFAULT_EXTEND_FACTOR);
    --ins;
    put(key, value);
  }

  @Override
  public String get(Integer key) {
    int index = getFromMap(new Triple(key, null, null));
    Triple temp = map[index];
    if (temp == null || temp.state == State.DELETED) {
      return null;
    }
    return temp.second;
  }

  @Override
  public void delete(Integer key) {

    ++del;

    int index = getFromMap(new Triple(key, "", State.Available));
    Triple temp = map[index];
    if (temp != null && temp.state == State.Available) {
      temp.state = State.DELETED;
      --size;
      int hashVal = hashFunction(key, 0);
      if (index != hashVal) {
        // --numCollisions;
      }
    }

  }

  private int getCollisions() {

    double x = 1;
    double y = numCollisions;

    return (int) Math.ceil(4058.615649 + 4.772063736e-1 * (x *= y) - 3.669683442e-6 * (x *= y)
        - 3.209527737e-11 * (x *= y) + 5.14250283e-16 * (x *= y) - 9.362775502e-22 * (x *= y));
  }

  @Override
  public boolean contains(Integer key) {
    int index = getFromMap(new Triple(key, null, null));
    if (index == -1) {
      return false;
    }
    Triple temp = map[index];
    if (temp == null || temp.state == State.DELETED) {
      return false;
    }
    return true;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public int capacity() {
    return capacity;
  }

  @Override
  public int collisions() {

    return getCollisions();
  }

  @Override
  public Iterable<Integer> keys() {
    return new IterableHashTable();
  }

  protected abstract int hashFunction(Integer key, int index);

  private int getFromMap(Triple toFind) {
    int index = 0;
    for (Triple t : map) {
      if (t != null && t.equals(toFind)) {
        return index;
      }
      ++index;
    }
    return -1;
  }

  private void extendTable(double factor) {
    if (factor <= 1 || capacity * factor == capacity) {
      throw new RuntimeException("wrong value for factor");
    }

    Triple[] tempMap = map;
    capacity = (int) (capacity * factor);
    size = 0;
    map = new Triple[capacity];

    numCollisions += capacity;

    for (Triple t : tempMap) {
      if (t == null || t.state == State.DELETED) {
        continue;
      }
      put(t.first, t.second);
      --ins;
    }
  }

  private enum State {
    DELETED, Available
  }

  private class Triple {
    private Integer first;
    private String second;
    private State state;

    public Triple(Integer first, String second, State state) {
      this.first = first;
      this.second = second;
      this.state = state;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof Triple)) {
        throw new RuntimeException("undefined operation");
      }
      return ((Triple) obj).first.equals(this.first);
    }
  }

  private class IterableHashTable implements Iterable<Integer>, Iterator<Integer> {

    private int nextAvailIndex;

    public IterableHashTable() {
      nextAvailIndex = getNextAvailIndex(-1);
    }

    @Override
    public boolean hasNext() {
      return nextAvailIndex != -1;
    }

    @Override
    public Integer next() {
      if (nextAvailIndex == -1) {
        return null;
      }

      Integer key = map[nextAvailIndex].first;
      nextAvailIndex = getNextAvailIndex(nextAvailIndex);
      return key;
    }

    @Override
    public Iterator<Integer> iterator() {
      return this;
    }

    private int getNextAvailIndex(int startingIndex) {
      for (int i = startingIndex + 1; i < capacity; ++i) {
        if (map[i] != null && map[i].state == State.Available) {
          return i;
        }
      }
      return -1;
    }

  }
}
