package eg.edu.alexu.csd.filestructure.hash.impl;

import java.util.Iterator;
import java.util.LinkedList;
import eg.edu.alexu.csd.filestructure.hash.IHash;
import eg.edu.alexu.csd.filestructure.hash.IHashChaining;

public class ChainingMap implements IHashChaining, IHash<Integer, String> {

  private final int DEFAULT_INITIAL_CAPACITY = 1200;

  private int size;
  private int capacity;
  private LinkedList<Pair>[] map;
  private int numCollisions;

  @SuppressWarnings("unchecked")
  public ChainingMap() {
    capacity = DEFAULT_INITIAL_CAPACITY;
    size = 0;
    numCollisions = 0;
    map = (LinkedList<Pair>[]) new LinkedList[DEFAULT_INITIAL_CAPACITY];
  }

  @Override
  public void put(Integer key, String value) {

    Pair entry = new Pair(key, value);
    int hashVal = hashFunction(key.hashCode());

    // key definitely not existing
    if (map[hashVal] == null) {
      map[hashVal] = new LinkedList<>();
      map[hashVal].add(entry);
      ++size;
      return;
    }

    Pair hashed = findInstance(entry, map[hashVal]);

    // not found
    if (hashed == null) {
      numCollisions += map[hashVal].size();
      map[hashVal].add(entry);
      ++size;
      return;
    }

    // key already mapped to a value, so change its value
    hashed.second = value;

  }

  @Override
  public String get(Integer key) {

    Pair temp = getHashedPair(key);

    // not found
    if (temp == null) {
      return null;
    }

    // found
    return temp.second;
  }

  @Override
  public void delete(Integer key) {
    Pair temp = getHashedPair(key);

    // not found
    if (temp == null) {
      return;
    }

    // found
    int hashVal = hashFunction(key.hashCode());
    map[hashVal].remove(temp);
    if (map[hashVal].size() == 0) {
      map[hashVal] = null;
    } else if (map[hashVal].size() == 1) {
      // numCollisions -= 2;
    } else {
      // --numCollisions;
    }
    --size;

  }

  @Override
  public boolean contains(Integer key) {

    Pair temp = getHashedPair(key);
    if (temp == null) {
      return false;
    }
    return true;
  }

  @Override
  public boolean isEmpty() {
    return size == 0;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public int capacity() {
    return capacity;
  }

  @Override
  public int collisions() {
    return numCollisions;
  }

  @Override
  public Iterable<Integer> keys() {
    return new IterableHashTable();
  }

  private int hashFunction(int key) {
    return key % capacity;
  }

  private Pair findInstance(Pair toFind, LinkedList<Pair> tableEntry) {

    if (tableEntry.contains(toFind)) {
      return tableEntry.get(tableEntry.indexOf(toFind));
    }

    return null;
  }

  private Pair getHashedPair(Integer key) {

    int hashVal = hashFunction(key.hashCode());
    LinkedList<Pair> tableEntry = map[hashVal];

    // not present
    if (tableEntry == null) {
      return null;
    }

    Pair temp = findInstance(new Pair(key, ""), tableEntry);

    // not found
    if (temp == null) {
      return null;
    }
    return temp;

  }

  private void extendTable(double factor) {
    if (factor <= 1 || capacity * factor == capacity) {
      throw new RuntimeException("wrong value for factor");
    }

    LinkedList<Pair>[] tempMap = map;
    capacity = (int) (capacity * factor);
    size = 0;
    map = new LinkedList[capacity];

    for (LinkedList<Pair> list : tempMap) {
      if (list == null) {
        continue;
      }
      for (Pair t : list) {
        put(t.first, t.second);
      }

    }
  }

  private class IterableHashTable implements Iterable<Integer>, Iterator<Integer> {

    private Iterator<Pair> currentValidEntry;
    private int currentMapIndex;

    public IterableHashTable() {
      currentMapIndex = nextValidEntry(-1);
      if (currentMapIndex != -1) {
        currentValidEntry = map[currentMapIndex].iterator();
      }
    }

    @Override
    public Iterator<Integer> iterator() {
      return this;
    }

    @Override
    public boolean hasNext() {
      if (currentMapIndex == -1) {
        return false;
      }

      if (currentValidEntry.hasNext()) {
        return true;
      }
      currentMapIndex = nextValidEntry(currentMapIndex);
      if (currentMapIndex == -1) {
        return false;
      }
      currentValidEntry = map[currentMapIndex].iterator();
      return true;
    }

    @Override
    public Integer next() {
      if (!hasNext()) {
        return null;
      }
      return currentValidEntry.next().first;
    }

    private int nextValidEntry(int startingIndex) {

      for (int i = startingIndex + 1; i < capacity; ++i) {
        if (map[i] != null) {
          return i;
        }
      }

      return -1;
    }

  }

}
