package eg.edu.alexu.csd.filestructure.hash.impl;

import eg.edu.alexu.csd.filestructure.hash.IHashDouble;

public class DoubleHashMap extends ProbedHash implements IHashDouble {

  private final int DEFAULT_PRIME = 1193;

  @Override
  protected int hashFunction(Integer key, int index) {
    int prime = DEFAULT_PRIME;
    int firstHash = key.hashCode() % capacity;
    int secondHash = prime - key.hashCode() % prime;
    return (int) ((firstHash + (long) secondHash * index) % capacity);
  }

}
