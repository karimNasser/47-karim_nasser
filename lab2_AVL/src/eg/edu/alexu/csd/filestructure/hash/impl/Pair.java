package eg.edu.alexu.csd.filestructure.hash.impl;

public class Pair {

  protected Integer first;
  protected String second;

  public Pair(Integer first, String second) {
    this.first = first;
    this.second = second;
  }

  @Override
  public boolean equals(Object obj) {

    if (!(obj instanceof Pair)) {
      throw new RuntimeException("undefined operation " + "for object of different type");
    }
    if (((Pair) obj).first.equals(first)) {
      return true;
    }
    return false;
  }

}
