package eg.edu.alexu.csd.filestructure.hash.impl;

import eg.edu.alexu.csd.filestructure.hash.IHashQuadraticProbing;

public class QuadProbMap extends ProbedHash implements IHashQuadraticProbing {

  private final long FIRST_CONST = 0;
  private final long SECOND_CONST = 1;

  @Override
  protected int hashFunction(Integer key, int index) {
    return (int) ((key.hashCode() + FIRST_CONST * index 
        + SECOND_CONST * index * index) % capacity);
  }

}
