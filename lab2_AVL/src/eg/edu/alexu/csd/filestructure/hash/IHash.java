package eg.edu.alexu.csd.filestructure.hash;

public interface IHash<K, V> {

  /** put key-value pair into the table. */
  void put(K key, V value);

  String get(K key);

  /** remove key (and its value) from table. */
  void delete(K key);

  /** return true if there is a value paired with key and false otherwise. */
  boolean contains(K key);

  /** return true if the table is empty. */
  boolean isEmpty();

  /** return the number of the active entries in the table. */
  int size();

  /** return the size of the table (including the empty slots). */
  int capacity();

  /** return the total number of collisions occurred so far. */
  int collisions();

  /** all keys in the table. */
  Iterable<K> keys();

}
