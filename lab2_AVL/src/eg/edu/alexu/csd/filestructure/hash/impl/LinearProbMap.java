package eg.edu.alexu.csd.filestructure.hash.impl;

import eg.edu.alexu.csd.filestructure.hash.IHashLinearProbing;

public class LinearProbMap extends ProbedHash implements IHashLinearProbing {

  @Override
  protected int hashFunction(Integer key, int index) {
    return (int) ((key.hashCode() + (long) index) % capacity);
  }

}
